import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MesagelistComponent } from './mesagelist.component';

describe('MesagelistComponent', () => {
  let component: MesagelistComponent;
  let fixture: ComponentFixture<MesagelistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MesagelistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MesagelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
