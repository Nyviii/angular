import { MessagesService } from './../../messagins/services/messages.service';
import { Component, Input, OnInit, Pipe } from '@angular/core';
import { Message } from '../models/message';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-mesagelist',
  templateUrl: './mesagelist.component.html',
  styleUrls: ['./mesagelist.component.css']
})
export class MesagelistComponent implements OnInit {

  @Input() messages!: Message[]
  @Input() count: number = 0

  constructor(
    private MessagesService:MessagesService
  ) {

   }

  ngOnInit(): void {
    this.messages = this.MessagesService.getAll();
    //gerer le nombre de message afficher
    /*if(this.count != 0){
      this.messages = this.messages.splice(0, this.count)
    }*/
  }

  /**
   * Si le checkbox est coché il écrase la liste total des messages
   * pour la remplacer par les messages uniquement vue (checkbox coché)
   * @param event (coché ? true : false)
   */
  isChecked(event: Event){
    //déclare l'event.target en tant qu'element HTML
    let input = event.target as HTMLInputElement;
    this.messages = this.messages.filter(message => message.read != input.checked)

  }

}
