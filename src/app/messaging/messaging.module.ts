import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notification/notification.component';
import { MesagelistComponent } from './mesagelist/mesagelist.component';
import { CreatemessageComponent } from './createmessage/createmessage.component';



@NgModule({
  declarations: [
    MesagelistComponent,
    NotificationComponent,
    CreatemessageComponent
  ],
  imports: [
    CommonModule,
  ], exports: [
    MesagelistComponent
  ]
})
export class MessagingModule { }
