export interface Message {

  //variable : type
  id?: number,
  from?: string,
  to?: string,
  subject?: string,
  body?: string,
  read?: boolean,

}
