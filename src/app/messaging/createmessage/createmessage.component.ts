import { Message } from './../models/message';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-createmessage',
  templateUrl: './createmessage.component.html',
  styleUrls: ['./createmessage.component.css']
})
export class CreatemessageComponent implements OnInit {

  message!: Message

  constructor() { }

  ngOnInit(): void {
    this.message = {
      id : 1,
      from : "prout",
      to : "non",
      subject: "carabistouille",
      body : "on aime la bite, ici",
      read : false,
    }
  }

  send() {
    console.log(this.message);
  }

  clear() {
    this.message = {}
  }
}
