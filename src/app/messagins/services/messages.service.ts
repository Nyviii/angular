import { Message } from './../../messaging/models/message';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
private messages:Message[] = [

]
  constructor(

  ) { }

  //retourne un tableau de messages
  getAll(): Message[]{
    return this.messages;
  }

  //retourne un message
  getById(id: number): Message | undefined{
    return this.messages.find(m=>m.id = id);
  }

  //ajout à la liste de messages : fait un traitement mais ne retourne rien
  create(message:Message): void{
    this.messages.push(message);
  }
}
