import { Component, Input, OnInit } from '@angular/core';
import { Message } from '../../messaging/models/message';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  mess!: Message[]

  constructor() { }

  ngOnInit(): void {
    this.mess = [{
      id: 1,
      from: 'Alexandre',
      to: 'Remi',
      subject: 'lorem',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed facilisis neque eget dictum mollis.',
      read: false,
    },{
      id: 2,
      from: 'Marine Le Pen',
      to: 'Ayoub',
      subject: 'Get out from my PAYS',
      body: 'Duis vel velit quis lectus sodales mollis ac et dui. Nulla facilisi.',
      read: true,
    },{
      id: 3,
      from: 'Franky',
      to: 'Vincent',
      subject: 'Hello my bro',
      body: 'Suspendisse mattis massa nibh, eget vestibulum nunc faucibus vel. Sed leo orci, placerat sed nisi non, cursus ultrices massa.',
      read: false,
    }]
  }

}
