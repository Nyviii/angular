import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MessagingModule } from './messaging/messaging.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TemplateModule } from './template/template.module';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TemplateModule,
    MessagingModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
