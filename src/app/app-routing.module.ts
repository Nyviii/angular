import { NgModule } from '@angular/core';
import { CreatemessageComponent } from './messaging/createmessage/createmessage.component';
import { MesagelistComponent } from './messaging/mesagelist/mesagelist.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { RouterModule, Routes } from '@angular/router';

const ROUTES = [
  {path: "", component: DashboardComponent},
  {path: "messages", component: MesagelistComponent},
  {path: "create-message", component: CreatemessageComponent}
] as Routes

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
